package com.deepan;

public class PaymentProcessingFactory {

    public PayPalGateway getPaypal(){
        return new PayPalGateway();
    }

    public StripeGateway getStripe(){
        return new StripeGateway();
    }

    public PaymentProcessor getPaymentProcessor(Gateway gateway){
        return new PaymentProcessor(gateway);
    }
}

//spring ioc container