package com.deepan;

public class PaymentProcessor {

    Gateway gateway;


    public PaymentProcessor(Gateway gateway){

        this.gateway = gateway;
    }

    public void makePayment(double amount){

//        10.234

        this.gateway.payment(amount);
    }

    public String retreiveTransactionStatus(int transactionID){

//        10

       return this.gateway.getTransactionStatus(transactionID);

    }
}


//
//ip address -- string
//looselu coupled
//1. create and inject the dependent (alternate to making the dependent compoenent fom creating its dependent on its own)
//2 abstract reference instead of concrete reference

//class and object



//data and functions

//state and behaviour