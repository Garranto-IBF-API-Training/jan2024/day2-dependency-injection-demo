package com.deepan;

public class StripeGateway implements Gateway {
    @Override
    public void payment(double amount) {
        System.out.println("Payment successfull using Stripe");
    }

    @Override
    public String getTransactionStatus(int id) {
        return "status pending - retreived by stripe gateway";
    }
}
