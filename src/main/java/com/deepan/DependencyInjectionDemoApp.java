package com.deepan;

public class DependencyInjectionDemoApp {
    public static void main(String[] args) {
        System.out.println("Dependency Injection Demo");
        System.out.println("============================");

        PaymentProcessingFactory factory = new PaymentProcessingFactory();
        StripeGateway stripe = factory.getStripe();
        PayPalGateway paypal = factory.getPaypal();
        PaymentProcessor paymentProcessor = factory.getPaymentProcessor(stripe);
        paymentProcessor.makePayment(1000);
        System.out.println(paymentProcessor.retreiveTransactionStatus(100));
    }
}


//payment processor

//paymentGatemey


//factory pattern

// StripeGateway stripe = new StripeGateway();
//        PayPalGateway payPal = new PayPalGateway();
//        PaymentProcessor paymentProcessor = new PaymentProcessor(payPal);
//        paymentProcessor.makePayment(1000);
//        System.out.println(paymentProcessor.retreiveTransactionStatus(100));


//inversion of control ---  framework taking control of components
//ApplicationContext --- Ioc container